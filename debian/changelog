prelude-lml (5.2.0-3) unstable; urgency=medium

  * d.patches: Add new patch
    - 008-Use-the-new-PCRE2-API.patch
      - Private patch from upstream, will be integrated. Migrate
        from PCRE3 to PCRE2 (Closes: #999982)
  * d.control: Bump Standards-Version to 4.6.0, no change

 -- Thomas Andrejak <thomas.andrejak@gmail.com>  Fri, 11 Mar 2022 12:53:39 +0200

prelude-lml (5.2.0-2) unstable; urgency=medium

  * d.patches: Add new patch
    - 023-Disable_GnuLib_Tests_perror2_strerror.patch
      - Apply patch from upstream, disable unportable tests, form armhf

 -- Thomas Andrejak <thomas.andrejak@gmail.com>  Thu, 26 Nov 2020 20:53:39 +0200

prelude-lml (5.2.0-1) unstable; urgency=medium

  * Bump version 5.2.0
    - d.control:
      - Update Prelude 5.2 dependencies
      - Bump debhelper version to 13
      - Bump Standards-Version to 4.5.1
    - New patchs:
      - 005-Use-pkgconfig-for-libprelude.patch
        - Update libprelude.m4
        - Use the new libprelude.m4 to detect libprelude with pkgconfig
      - 006-fix-gnutls-deps.patch
        - Fix gnutls detection in configure
    - d.p.*.patch: Set the field Forwarded
    - d.copyright: CS SI to CS GROUP
    - Update d.u.metadata: add missing fields
    - d.watch: bump version to 4

 -- Thomas Andrejak <thomas.andrejak@gmail.com>  Tue, 17 Nov 2020 20:53:39 +0200

prelude-lml (5.1.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Thomas Andrejak <thomas.andrejak@gmail.com>  Tue, 12 Nov 2019 12:07:24 +0200

prelude-lml (5.1.0-1) experimental; urgency=medium

  * Bump version 5.1.0
    - The new version fixed ftbfs with GCC-9 (Closes: #925804)
  * d/control:
    - Migrate from debhelper to debhelper-compat
    - Update Standard-Version to 4.4.1
    - Add Rules-Requires-Root to no
    - Bump DebHelper to 12
    - Bump prelude dependencies to 5.1
  * Update copyrights dates
  * Add new patch:
    - 003-fix-test_rwlock1.patch
    - 004-fix_thread_create.patch

 -- Thomas Andrejak <thomas.andrejak@gmail.com>  Fri, 11 Oct 2019 09:00:00 +0200

prelude-lml (4.1.0-2) unstable; urgency=medium

  * Fixing #919869 issue: purge removed too many files
  * Bump Standards-Version to 4.3.0
  * Remove trailing whitespace in d/changelog
  * Improve hardening of service files
  * Add d/upstream/metadata file

 -- Thomas Andrejak <thomas.andrejak@gmail.com>  Sat, 23 Feb 2019 08:15:47 +0100

prelude-lml (4.1.0-1) unstable; urgency=medium

  * Bump version 4.1.1
    - Update dependencies: libprelude 4.1 is required and also prelude-utils
  * d/control:
    - Update description
    - Update Vcs-* fields to salsa
    - Bump Standards-Version to 4.1.3
    - Bump to debhelper and compat 11
  * Update d/rules and remove d/prelude-lml.postinst to use new systemd
    helpers
  * Update copyrights dates

 -- Thomas Andrejak <thomas.andrejak@gmail.com>  Thu, 15 Mar 2018 08:15:47 +0100

prelude-lml (3.1.0-2) unstable; urgency=medium

  * Push to unstable
  * Bump Standards-Version to 4.1.2, no changes needed
  * Remove rulesets since they are now in prelude-lml-rules
    package (Closes: #884415)

 -- Thomas Andrejak <thomas.andrejak@gmail.com>  Sun, 17 Dec 2017 21:15:47 +0100

prelude-lml (3.1.0-1) experimental; urgency=medium

  * Updating rules file:
    - Add systemd support
    - Reoganize dh call
    - Add missing creation of /var/spool/prelude/prelude-lml
  * Addition of the upstream signing key
  * Update of watch file:
    - Update URL since upstream website moved
    - Add GPG check
  * Migrate from init.d to systemd
    - Remove init script and add .maintscript to handle update
    - Add service and tmpfile
    - Update postrm and rename it to prelude-lml.postrm
    - Add prelude-lml.postinst to add creation of tmpfile
  * Remove unnecessary dirs file
  * Update README.debian
  * New upstream release 3.1.0 (Closes: #878607)
  * Patches:
    - Remove unnecessary patches now that upstream included them
    - 001-debian_log_paths: Update this patch for new prelude-lml version
    - 002-Fix_spellcheck.patch: Fix spelling inside prelude-lml binary
  * Control:
    - Add Thomas Andrejak as uploader
    - Remove Mickael Profeta from uploaders (Closes: #838925)
    - Remove hardcoded Pre-Depends on multiarch-support (Closes: #870559)
    - Remove rsyslog from Recommends
    - Remove quilt build-dep
    - Change Priority from extra to optional
    - Add Vcs fields
    - Bump Standards-Version to 4.1.1
    - Bump debhelper compat level to 10
    - Set Homepage field
  * Rewriting of copyright file to fit the machine-readable format
  * Add missing copyright information
  * Add LML rules that are now shipped with prelude-lml-rules
  * Fix some trailing whitespaces in changelog

 -- Thomas Andrejak <thomas.andrejak@gmail.com>  Sun, 12 Nov 2017 23:15:47 +0100

prelude-lml (1.0.0-5.3) unstable; urgency=medium

  * Non-maintainer upload.
  * Brown paper bag release.
  * Really add pkg-config to b-d.

 -- Andreas Metzler <ametzler@debian.org>  Sat, 30 Aug 2014 16:54:32 +0200

prelude-lml (1.0.0-5.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Configure with ICU_CFLAGS="$(shell pkg-config --cflags icu-i18n)" and
    ICU_LIBS="$(shell pkg-config --libs icu-i18n)" instead of letting
    ./configure use icu-config. The latter prints out icu's compile-time
    cflags including -fPIE (see #759792) which causes a build-error on amd64
    et al. Closes: #759247

 -- Andreas Metzler <ametzler@debian.org>  Sat, 30 Aug 2014 15:04:09 +0200

prelude-lml (1.0.0-5.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rebuild against GnuTLS 3. Drop build-dependency on libgnutls-dev, instead
    rely on libprelude-dev pulling in GnuTLS (unused by prelude-lml) and
    gcrypt development packages.

 -- Andreas Metzler <ametzler@debian.org>  Fri, 15 Aug 2014 07:51:05 +0200

prelude-lml (1.0.0-5) unstable; urgency=low

  * Bump Standards Version to 3.9.4
  * Refreshed quilt patches
  * Fix FTBFS with eglibc 2.17 (Closes: #701411)

 -- Pierre Chifflier <pollux@debian.org>  Tue, 02 Jul 2013 15:19:08 +0200

prelude-lml (1.0.0-4) unstable; urgency=high

  * Disable tests to avoid build failure on on kFreeBSD (Closes: #677852)
  * Urgency high, RC bug

 -- Pierre Chifflier <pollux@debian.org>  Mon, 18 Jun 2012 21:30:19 +0200

prelude-lml (1.0.0-3) unstable; urgency=high

  * Trigger rebuild (Closes: #676028)
  * Bump Standards Version to 3.9.3
  * Switch to dpkg-source 3.0 (quilt) format
  * Switch to DH version 9
    - Enable hardening options
    - Enable multi-arch
  * Urgency high, RC bugs

 -- Pierre Chifflier <pollux@debian.org>  Sat, 16 Jun 2012 13:38:01 +0200

prelude-lml (1.0.0-2) unstable; urgency=low

  * Fix FTBFS with undefined symbol lt__PROGRAM__LTX_preloaded_symbols
    (Closes: #622046)
  * Bump Standards Version to 3.9.2
  * Ensure init script messages have newlines (Closes: #574595)
  * Exit init script gracefuly if profile does not exist
    (Closes: #616178)

 -- Pierre Chifflier <pollux@debian.org>  Mon, 02 May 2011 13:55:25 +0200

prelude-lml (1.0.0-1) unstable; urgency=low

  * Imported Upstream version 1.0.0

 -- Pierre Chifflier <pollux@debian.org>  Thu, 18 Mar 2010 09:45:21 +0100

prelude-lml (1.0.0~rc2-1) unstable; urgency=low

  * New upstream release

 -- Pierre Chifflier <pollux@debian.org>  Tue, 09 Feb 2010 13:30:03 +0100

prelude-lml (1.0.0~rc1-1) unstable; urgency=low

  * New upstream release
  * Bump standards version to 3.8.4
  * Update description
  * Recommend rsyslog | system-log-daemon

 -- Pierre Chifflier <pollux@debian.org>  Wed, 03 Feb 2010 11:45:37 +0100

prelude-lml (0.9.15-1) unstable; urgency=low

  * New Upstream Version
  * Bump standards version to 3.8.2
  * Set debconf compat level to 5

 -- Pierre Chifflier <pollux@debian.org>  Fri, 17 Jul 2009 11:23:11 +0200

prelude-lml (0.9.14-2) unstable; urgency=low

  * Upload to unstable

 -- Pierre Chifflier <pollux@debian.org>  Thu, 26 Feb 2009 22:59:35 +0100

prelude-lml (0.9.14-1) experimental; urgency=low

  * New upstream release

 -- Pierre Chifflier <pollux@debian.org>  Sun, 19 Oct 2008 22:44:21 +0200

prelude-lml (0.9.13-1) experimental; urgency=low

  * New upstream release

 -- Pierre Chifflier <pollux@debian.org>  Mon, 25 Aug 2008 16:15:29 +0200

prelude-lml (0.9.12.2-2) unstable; urgency=low

  * Update watch file
  * Bump standards version (no changes)

 -- Pierre Chifflier <pollux@debian.org>  Tue, 01 Jul 2008 11:51:33 +0200

prelude-lml (0.9.12.2-1) unstable; urgency=low

  * New upstream release (fix installation directory of rules)

 -- Pierre Chifflier <pollux@debian.org>  Thu, 24 Apr 2008 21:20:56 +0200

prelude-lml (0.9.12.1-1) unstable; urgency=low

  * New upstream release

 -- Pierre Chifflier <pollux@debian.org>  Wed, 23 Apr 2008 19:17:28 +0200

prelude-lml (0.9.11-1) unstable; urgency=low

  * New upstream release
  * drop disable_cron, merged upstream

 -- Pierre Chifflier <pollux@debian.org>  Mon, 17 Dec 2007 19:09:21 +0100

prelude-lml (0.9.10.1-3) unstable; urgency=low

  * Remove remaining rules and var files on purge (Closes: #355737, #455030)
  * Bump standard version (no changes)

 -- Pierre Chifflier <pollux@debian.org>  Sun, 16 Dec 2007 16:52:31 +0100

prelude-lml (0.9.10.1-2) unstable; urgency=low

  * Add quilt patches:
    + debian_log_paths: set correct path for debian logs (auth.log, apache)
    + disable_cron: disable cron alerts by default (see README.Debian)

 -- Pierre Chifflier <pollux@debian.org>  Mon, 15 Oct 2007 17:46:01 +0200

prelude-lml (0.9.10.1-1) unstable; urgency=low

  * New upstream release
  * Update my email address

 -- Pierre Chifflier <pollux@debian.org>  Wed, 08 Aug 2007 22:05:39 +0200

prelude-lml (0.9.10-1) unstable; urgency=low

  * New upstream release

 -- Pierre Chifflier <chifflier@inl.fr>  Sun, 20 May 2007 16:07:12 +0200

prelude-lml (0.9.9-1) unstable; urgency=low

  * New upstream release
  * Update my email address
  * Add watch file
  * Add compat file

 -- Pierre Chifflier <chifflier@inl.fr>  Wed, 02 May 2007 14:13:54 +0200

prelude-lml (0.9.8.1-1) unstable; urgency=low

  * New upstream release
  * Add myself to Uploaders

 -- Pierre Chifflier <chifflier@cpe.fr>  Mon, 29 Jan 2007 22:52:19 +0100

prelude-lml (0.9.7-1) unstable; urgency=low

  * New upstream release

 -- Mickael Profeta <profeta@debian.org>  Fri, 27 Oct 2006 10:38:47 +0200

prelude-lml (0.9.4-1) unstable; urgency=low

  * New upstream release
  * Modify copyright to include LGPL for libmissing directory

 -- Mickael Profeta <profeta@debian.org>  Wed, 26 Apr 2006 13:49:31 +0200

prelude-lml (0.9.2-1) unstable; urgency=low

  * New upstream release

 -- Mickael Profeta <profeta@debian.org>  Sat,  4 Feb 2006 17:15:22 +0100

prelude-lml (0.9.0-2) unstable; urgency=low

  * update dependencies (closes: #343512)

 -- Mickael Profeta <profeta@debian.org>  Thu, 15 Dec 2005 22:57:56 +0100

prelude-lml (0.9.0-1) unstable; urgency=low

  * New upstream release
  * new config.guess/config.sub (closes: #333649)

 -- Mickael Profeta <profeta@debian.org>  Wed,  5 Oct 2005 13:26:41 +0000

prelude-lml (0.8.6-4) unstable; urgency=low

  * added libssl-dev in build-depend

 -- Mickael Profeta <profeta@debian.org>  Wed, 12 Nov 2003 16:15:54 +0100

prelude-lml (0.8.6-3) unstable; urgency=low

  * change == operator to -eq in init file

 -- Mickael Profeta <profeta@debian.org>  Wed, 12 Nov 2003 11:46:15 +0100

prelude-lml (0.8.6-2) unstable; urgency=low

  * Change the maintainer in control file

 -- Mickael Profeta <profeta@debian.org>  Tue,  4 Nov 2003 15:06:40 +0100

prelude-lml (0.8.6-1) unstable; urgency=low

  * New upstream release
  * Add in copyright exception to GPL in order to link with OpenSSL

 -- Mickael Profeta <profeta@debian.org>  Tue,  4 Nov 2003 10:19:57 +0100

prelude-lml (0.8.3-1) unstable; urgency=low

  * New upstream release

 -- Mickael Profeta <mike@alezan.org>  Sun, 12 Oct 2003 22:08:03 +0200

prelude-lml (0.8.2-1) unstable; urgency=low

  * New upstream release

 -- PROFETA Mickael <profeta@debian.org>  Sun,  5 Jan 2003 21:17:38 +0100

prelude-lml (0.8.1-1) unstable; urgency=low

  * Initial Release.

 -- Thomas Seyrat <tomasera@debian.org>  Sat,  6 Apr 2002 19:37:00 +0200
